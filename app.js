const express = require('express');
const bodyParser = require('body-parser');
const handlebars=require('express-handlebars');
//const { urlencoded } = require('body-parser');
const app = express();

const crud=require('crud');
let cruds=new crud();

const urlencodeParser=bodyParser.urlencoded({extended:false});


let port = process.env.PORT || 3000;

//liberando o diretorio
app.use('/img', express.static('img'));
app.use('/css', express.static('css'));
app.use('/js', express.static('js'));

//sql.query("use crude-node");

const hbs = handlebars.create({ defaultLayout:'main' });

app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');


app.get('/', function (req, res) {
    res.render('index');
});


//routes and templates  
app.get("/",function(req,res){
    //res.send("teste");
    //res.sendFile(__dirname+"/index.html");
    //res.render('index');
    //console.log(req.params.id)
    res. render('index')
});

app.get("/inserir",function(req,res){res.render("inserir");});

app.get("/select/:id?",function(req,res){cruds.read(req,res);});

app.post("/controllerForm",urlencodeParser,function(req,res){cruds.create(req,res);});

app.get('/deletar/:id',function(req,res){cruds.delete(req,res);});

app.get("/update/:id",function(req,res){cruds.update(req,res);});

app.post("/controllerUpdate",urlencodeParser,function(req,res){cruds.update(req,res,'controller');});

//Start server
app.listen(port);